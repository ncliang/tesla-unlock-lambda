# tesla-unlock-lambda

Quick AWS lambda function that lets you unlock a Tesla remotely. Requires 2 environment variables: `TESLA_EMAIL` and `TESLA_PASS` to be configured in the Lambda function config for this to work.

You will need to use the Lambda ZIP upload which contains `index.js` and `node_modules` in the top level of the ZIP for this to work as it depends on the TeslaJS library. If you haven't already, make sure you run `npm install` to build the `node_modules` directory before creating the archive.