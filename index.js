 var tjs = require('teslajs');

 var username = process.env.TESLA_EMAIL;
 var password = process.env.TESLA_PASS;

 exports.handler = (event, context, callback) => {
     tjs.login(username, password, function(err, result) {
         if (result.error) {
             console.log(JSON.stringify(result.error));
             process.exit(1);
         }

         var token = JSON.stringify(result.authToken);

         if (token)
             console.log("Login Succesful!");

         var options = {
             authToken: result.authToken
         };
         tjs.vehicles(options, function(err, vehicle) {
             console.log("Vehicle " + vehicle.vin + " is: " + vehicle.state);
             var options = {
                 authToken: result.authToken,
                 vehicleID: vehicle.id_s
             };

             if (event.clickType === 'SINGLE') {
                 tjs.doorUnlock(options, function(err, result) {
                     if (err) {
                         console.log("Door unlock failed!".red);
                         return;
                     }

                     console.log("\nThe doors are now: " + "UNLOCKED");
                     callback();
                 });
             } else if (event.clickType === 'DOUBLE') {
                 tjs.openTrunk(options, tjs.FRUNK, function(err, result) {
                     if (err) {
                         console.log("Frunk open failed!".red);
                         return;
                     }

                     console.log("\nThe frunk is now: " + "OPEN");
                     callback();
		 });
	     }
	 });

     });
 }

